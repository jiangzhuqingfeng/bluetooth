package com.jzqf.bluetooth;

import org.junit.Test;

public class UtilTest {

    @Test
    public void string() {
        //5E:47:7B:00:D8:75
        //62长度字节
        String s8 = "02" +//数据单元长度2字节
                "01" +//普通发现模式
                "1a" +
                "0a" +//数据单元长度10字节
                "09" +//Local Name  简称
                "47616c617879205338" +//设备名称 Galaxy S8  空格=20
                "02" +//数据单元长度为2?
                "0af9" +
                "020af9" +
                "11" +//数据单元17字节
                "07" +//完整的 128 bit UUID 列表  128 bit = 16 字节
                "776a6d20808ae691a7e79daae7829ce8" +//SERVICE_UUID 16字节
                "000000000000000000000000000000000000000000000000";//补位补足62字节
        System.out.println("776a6d20808ae691a7e79daae7829ce8".length());
        String mate30 = "02" +//长度
                "01" +//普通发现模式
                "01" +
                "09" +//长度9
                "09" +//Local Name 简称
                "4d61746533304a46" +//Mate30JF
                "020afc" +
                "020afc" +
                "11" +//17字节
                "07" +//SERVICE UUID 的AD TYPE
                "33905c25b7c4668f82eedd75408de752" +//SERVICE_UUID 16字节
                "00000000000000000000000000000000000000000000000000";//补位补足62字节
        System.out.println("33905c25b7c4668f82eedd75408de752".length());
    }
}
