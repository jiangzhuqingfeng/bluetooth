package com.jzqf.bluetooth

import android.app.Application
import com.tencent.bugly.crashreport.CrashReport

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        CrashReport.initCrashReport(this, "60f1e4607c", true)
    }
}