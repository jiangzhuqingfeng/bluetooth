package com.jzqf.bluetooth.client

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.jzqf.bluetooth.base.BaseViewBindingAdapter
import com.jzqf.bluetooth.base.BaseViewBindingHolder
import com.jzqf.bluetooth.databinding.ItemMenuBinding

class MenuAdapter : BaseViewBindingAdapter<String>() {
    override fun getViewBinding(
        inflater: LayoutInflater?,
        parent: ViewGroup?,
        viewType: Int
    ): ViewBinding {
        return ItemMenuBinding.inflate(inflater!!, parent, false)
    }

    override fun bindData(
        holder: BaseViewBindingHolder?,
        viewBinding: ViewBinding?,
        model: String?,
        position: Int
    ) {
        val itemMenuBinding = viewBinding as ItemMenuBinding
        itemMenuBinding.itemBtn.setText(model)
    }
}