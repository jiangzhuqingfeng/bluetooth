package com.jzqf.bluetooth

import com.google.gson.Gson

object Constants {
    const val CONNECT_SOCKET = "SOCKET连接"
    const val SEND_SOCKET = "SOCKET发送"
    const val CLOSE_SOCKET = "SOCKET断开"

    const val CONNECT_BLE = "BLE连接"
    const val SEND_BLE = "BLE发送"
    const val CLOSE_BLE = "BLE断开"
    const val SERVICE = "服务端"


    fun data(count: Int): String {
        val list = mutableListOf<String>()
        for (i in 0..count) {
            list.add("魑魅魍魉惊本身$i")
        }
        return Gson().toJson(list)
    }
}