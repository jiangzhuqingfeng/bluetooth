package com.jzqf.bluetooth.event

object EventCode {
    const val RESULT = 1001

    const val CONNECT = 1002

    const val SEND_DATA = 1003

    const val DISCONNECT = 1004
}