package com.jzqf.bluetooth.base;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

public class BaseViewBindingHolder extends RecyclerView.ViewHolder {
    ViewBinding viewBinding;
    private OnItemChildClickListener mOnItemChildClickListener;

    public BaseViewBindingHolder(@NonNull ViewBinding viewBinding) {
        super(viewBinding.getRoot());
        this.viewBinding = viewBinding;
    }

    public interface OnItemChildClickListener {
        void onItemChildClick(View childView);
    }

    public void setOnItemChildClickListener(OnItemChildClickListener onItemChildClickListener) {
        this.mOnItemChildClickListener = onItemChildClickListener;
    }

    public void addOnClickListener(View view) {
        if (view != null) {
            view.setOnClickListener(v -> {
                if (mOnItemChildClickListener != null) {
                    mOnItemChildClickListener.onItemChildClick(v);
                }
            });
        }
    }
}
