package com.jzqf.bluetooth.base;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

/**
 * 使用viewbinding的adapter
 * 支持多条目布局：
 * 支持header和footer
 *
 * @param <T>
 */
public abstract class BaseViewBindingAdapter<T> extends
        RecyclerView.Adapter<BaseViewBindingHolder> {
    private List<T> mData;
    private OnItemChildClickListener mOnItemChildClickListener;

    public BaseViewBindingAdapter() {
        mData=new ArrayList<>();
    }

    @NonNull
    @Override
    public BaseViewBindingHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewBinding viewBinding = getViewBinding(LayoutInflater.from(parent.getContext()), parent, viewType);
        return new BaseViewBindingHolder(viewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewBindingHolder holder, int position) {
        bindData(holder, holder.viewBinding, getItem(position), position);
        holder.setOnItemChildClickListener(childView -> {
            if (mOnItemChildClickListener != null) {
                mOnItemChildClickListener.onItemChildClick(childView, getItemViewType(position), position);
            }
        });
        holder.addOnClickListener(holder.viewBinding.getRoot());
    }

    protected abstract ViewBinding getViewBinding(LayoutInflater inflater, ViewGroup parent, int viewType);

    protected abstract void bindData(BaseViewBindingHolder holder, ViewBinding viewBinding, T model, int position);

    /**
     * 添加数据
     */
    public void notifyData(List<T> list) {
        if (list != null) {
            mData.clear();
            mData.addAll(list);
            notifyDataSetChanged();
        } else {
            mData.clear();
            notifyDataSetChanged();
        }
    }

    public void append(List<T> response) {
        if (response != null) {
            mData.addAll(response);
        }
        notifyDataSetChanged();
    }

    /**
     * 根据索引移除对象
     *
     * @param index 索引
     */
    public void remove(int index) {
        mData.remove(index);
        notifyDataSetChanged();
    }

    /**
     * 移除指定对象
     *
     * @param object 对象
     */
    public void remove(T object) {
        mData.remove(object);
        notifyDataSetChanged();
    }

    /**
     * 清空列表
     */
    public void clear() {
        mData.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public T getItem(int position) {
        return mData.get(position);
    }

    public void setOnItemChildClickListener(OnItemChildClickListener onItemChildClickListener) {
        this.mOnItemChildClickListener = onItemChildClickListener;
    }

    /**
     * item 点击事件
     */
    public interface OnItemChildClickListener {

        /**
         * item 被点击
         *
         * @param itemView 被点击的 {@link View}
         * @param viewType 布局类型
         * @param position 在 RecyclerView 中的位置
         */
        void onItemChildClick(@NonNull View itemView, int viewType, int position);
    }

}
