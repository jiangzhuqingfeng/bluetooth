package com.jzqf.bluetooth.base

import android.Manifest
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.tbruyelle.rxpermissions3.RxPermissions

/**
 * @author LiuMin
 * 2019-12-24 10:22
 * Description:Activity基类
 */
abstract class BaseActivity<VB : ViewBinding> : AppCompatActivity() {
    lateinit var mViewBinding: VB
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //减少window图层overdraw
        window.setBackgroundDrawable(null)
        // 默认软键盘不弹出
        window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN
                    or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED
        )
        mViewBinding = getViewBinding()
        setContentView(mViewBinding.root)
        requestPermission()
    }

    private fun requestPermission() {
        val rxPermissions = RxPermissions(this)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            rxPermissions.request(
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_NETWORK_STATE
            )
                .subscribe {
                    if (it) {
                        Toast.makeText(this, "已授权", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "请授权", Toast.LENGTH_SHORT).show()
                    }
                }
        } else {
            rxPermissions.request(
                Manifest.permission.BLUETOOTH_ADMIN,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_NETWORK_STATE
            )
                .subscribe {
                    if (it) {
                        Toast.makeText(this, "已授权", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "请授权", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }

    abstract fun getViewBinding(): VB

    override fun onDestroy() {
        super.onDestroy()
    }


}