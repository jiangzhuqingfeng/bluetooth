package com.jzqf.bluetooth

import java.nio.charset.Charset
import java.util.*

object Api {
    val charset: Charset = Charsets.UTF_8

    const val searchList = "查询分页数据"

    const val disconnect = "断开连接"

    /**
     * 三星:65:57:3F:31:33:19  7A:3A:5E:26:58:AE
     * 人脸:22:22:BA:CF:75:6D
     * Mate30:E4:FD:A1:91:41:84
     * 荣耀:24:DA:33:50:03:EE
     * ioface: AB:89:67:45:23:01
     */
    const val address = "AB:89:67:45:23:01"
    val SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")


    //蓝牙设备的notify的UUID
    val UUID_NOTIFY = UUID.fromString("e89c82e7-aa9d-e7a7-91e6-8a80206d6a66")
    /**
     * 蓝牙设备的Service的UUID
     */
    val UUID_SERVICE = UUID.fromString("52e78d40-75dd-ee82-8f66-c4b7255c9033")
    /**
     * 特征值读权限的UUID
     */
    val UUID_CHARACTERISTIC_READ = UUID.fromString("e89c82e7-aa9d-e7a7-91e6-8a80206d6a63")

    /**
     * 特征值写权限的UUID
     */
    val UUID_CHARACTERISTIC_WRITE = UUID.fromString("53162105-f015-f721-7834-638288253266")

    val UUID_DESCRIPTOR = UUID.fromString("e89c82e7-aa9d-e7a7-91e6-8a80206d6a62")

    /**
     * 传输汉字个数  字节数/3向下取整
     *
     */
    const val MAX_MTU = 170

    /**
     * 发送延时 毫秒
     */
    const val DELAY_SEND = 10L

    /**
     * 传输开始标识符
     */
    const val FLAG_START = "START"
    const val FLAG_END = "END"

    /**
     * 蓝牙设备最远距离 单位:米
     */
    const val MAX_DISTANCE = 10
}